﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceGUI : MonoBehaviour {

	[SerializeField] Texture cross;

	PlayerControler playerControler;

	void Start() {
		playerControler = (PlayerControler)gameObject.GetComponent ("PlayerControler");
	}

	void OnGUI() {
		GUI.DrawTexture(new Rect (Screen.width * 0.5f, Screen.height * 0.5f, 20, 20), cross);

		GUI.Box (new Rect (30, Screen.height - 60 ,200 / playerControler.MaxHealth * playerControler.CurHealth, 30), playerControler.CurHealth.ToString());
	}
}
