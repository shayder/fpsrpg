﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour {

	[SerializeField] int MaxHealth = 30;
	[SerializeField] int curHealth;

	public int CurHealth
	{
		get{ return curHealth; }
		set{curHealth = value; }
	}
	void Start () {
		curHealth = MaxHealth;
	}

	public void ChangeHealth(int value) {
		CurHealth += value;

		if (CurHealth <= 0) {
			Destroy (gameObject);
		}
	}
}
