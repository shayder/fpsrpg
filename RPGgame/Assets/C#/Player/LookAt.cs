﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour {

	Transform thisTransform;
	Camera playerCamera;
	RaycastHit hit;

	float lookatDist;

	// Use this for initialization
	void Start () {
		playerCamera = Camera.main;
		thisTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (Physics.Raycast (playerCamera.transform.position, transform.forward, out hit, Mathf.Infinity)) {
			lookatDist = Vector3.Distance(thisTransform.position, hit.collider.gameObject.transform.position);
			if (lookatDist <= 2.6f && hit.collider.gameObject.tag == "NPC") {
				Debug.Log(lookatDist);
			}

		}
	}
}
