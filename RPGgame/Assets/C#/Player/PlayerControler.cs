﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour {
	Transform myTransform;
	Camera camera;

	[SerializeField] int maxHealth = 100;
	[SerializeField] int curHealth = 0;
	[SerializeField] float walkSpeed = 5.0f;
	[SerializeField] float runSpped = 8.0f;
	[SerializeField] float rotateSpeed = 5.0f;

	float curMovespeed;
	float rotationX;
	float rotationY;

	public int MaxHealth
	{
		get { return maxHealth;}
	}
	public int CurHealth {
		get { return curHealth;}
	}

	void Start () {
		myTransform = transform;
		camera = Camera.main;
		curHealth = maxHealth;
		curMovespeed = walkSpeed;
	}

	void Update () {
		playerMovement();
		PlayerRotation ();
	}

	void playerMovement() 
	{
		if (Input.GetKey(KeyCode.W)) {
			myTransform.position += myTransform.forward * curMovespeed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.S)) {
			myTransform.position -= myTransform.forward * curMovespeed * Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.D)) {
			myTransform.position += myTransform.right * curMovespeed * Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.A)) {
			myTransform.position -= myTransform.right * curMovespeed * Time.deltaTime;
		}
	}
	void  PlayerRotation() {
		rotationX = myTransform.localEulerAngles.y + Input.GetAxis ("Mouse X") * rotateSpeed * Time.deltaTime;

		rotationY += Input.GetAxis ("Mouse Y") * rotateSpeed * Time.deltaTime;
		rotationY = Mathf.Clamp(rotationY, -50, 50);

		camera.transform.localEulerAngles = new Vector3 (-rotationY, 0, 0);
		myTransform.localEulerAngles = new Vector3 (0, rotationX, 0);
	}
}
