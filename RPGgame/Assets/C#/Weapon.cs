﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

	float shootTimer;
	public float setShootTimer = 1.0f;

	// var for raycast
	Vector3 accuracyDir;
	RaycastHit hit;
	GameObject gameObjecthit;

	//Weapon stats
	int minDamage = 12;
	int maxDamage = 17;
	int currentDamage;
	int maxAmmo = 25;
	int currentAmmo;
	float setFirerate = 0.2f;
	float firerate;
	float reloadTime = 2.0f;
	float accuracy = 0.02f;

	Camera playerCamera;

	// Use this for initialization
	void Start () {
		playerCamera = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		if (CanFire (ref firerate, setFirerate) && Input.GetMouseButtonDown(0)) {
			Shoot();
			setFirerate = 0;
		}
	}
	private bool CanFire(ref float timer, float setTimer) {
		if (timer < setTimer) {
			timer += Time.deltaTime;
		}
		if (timer >= setTimer) {
			return true;
		}
		return false;
	}
	private void Shoot() {
		gameObjecthit = ObjectHit(accuracy);
		Debug.Log (gameObjecthit);
		if (gameObjecthit != null && gameObjecthit.tag == "Enemy") {
			currentDamage = Random.Range (minDamage, maxDamage + 1);
			Debug.Log (currentDamage);
			EnemyStats es = (EnemyStats)hit.collider.gameObject.GetComponent("EnemyStats");
			es.ChangeHealth(-currentDamage);
		}
	}
	GameObject ObjectHit(float rayAccuracy) {
		accuracyDir = transform.forward;
		if (rayAccuracy != 0) {
			accuracyDir.x += Random.Range(-accuracy, accuracy);
			accuracyDir.y += Random.Range(-accuracy, accuracy);
		}
		DrawRaycast (accuracyDir);
		if (Physics.Raycast (playerCamera.transform.position, accuracyDir, out hit, Mathf.Infinity)) {
			return hit.collider.gameObject;
		}
		return null;
	}

	//debug class

	void DrawRaycast(Vector3 dir) {
		
		Debug.DrawRay(playerCamera.transform.position, dir * 100, Color.red, 1.0f, true);
	}

}
