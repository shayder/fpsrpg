﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneSwitcher : MonoBehaviour {

	[SerializeField] private int levelNumber = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter(Collider c) {
		if (c.gameObject.tag == "Player") {
			Application.LoadLevel(levelNumber);
		}
	}
}
